
2.) Answers
/*	a. List the books Authored by Marjorie Green.
		BU1032 - The Busy Executive's Database Guide
		BU2075 - You Can Combat Computer Stress!

	b. List the books Authored by Michael O'Leary.
		BU1111 - Cooking With Computers

	c. Write the author/s of "The Busy Executives Database Guide".
		213-46-8915 Marjorie Green
		409-56-7008 Abraham Bennet

	d. Identify the publisher of "But Is It User Friendly?".
		1389 - Algodata Infosystems

	e. List the books published by Algodata Infosystems.
		BU1032 - The Busy Executive's Database Guide
		BU1111 - Cooking With Computers
		BU7832 - Straight Talk About Computers
		PC1035 - But Is It User Friendly?
		PC8888 - Secrets of Silicon Valley
		PC9999 - Net Etiquette

*/

3.)
	
	mysql -u root

	-- Create database
	CREATE DATABASE blog_db;
	
	SHOW DATABASES;
	USE blog_db;

	--users
	CREATE TABLE users ( id INT NOT NULL AUTO_INCREMENT, email VARCHAR (100) NOT NULL, password VARCHAR (300) NOT NULL, datetime_created DATETIME NOT NULL, PRIMARY KEY (id));
	SHOW TABLES;
	DESCRIBE users;

	--posts
	CREATE TABLE posts ( id INT NOT NULL AUTO_INCREMENT, author_id INT NOT NULL, title VARCHAR (500) NOT NULL, content VARCHAR (5000) NOT NULL, datetime_posted DATETIME NOT NULL, PRIMARY KEY (id), CONSTRAINT fk_posts_author_id FOREIGN KEY (author_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
	SHOW TABLES;
	DESCRIBE posts;

	--post_likes
	CREATE TABLE post_likes ( id INT NOT NULL AUTO_INCREMENT, post_id INT NOT NULL, user_id INT NOT NULL, datetime_liked DATETIME NOT NULL, PRIMARY KEY (id), CONSTRAINT fk_post_likes_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT, CONSTRAINT fk_post_likes_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
	SHOW TABLES;
	DESCRIBE post_likes;

	--post_comments
	CREATE TABLE post_comments ( id INT NOT NULL AUTO_INCREMENT, post_id INT NOT NULL, user_id INT NOT NULL, content VARCHAR (5000) NOT NULL, datetime_commented DATETIME NOT NULL, PRIMARY KEY (id), CONSTRAINT fk_post_comments_post_id FOREIGN KEY (post_id) REFERENCES posts(id) ON UPDATE CASCADE ON DELETE RESTRICT, CONSTRAINT fk_post_comments_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE RESTRICT);
	SHOW TABLES;
	DESCRIBE post_comments;
